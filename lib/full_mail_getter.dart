import 'dart:async';

import 'package:meta/meta.dart';
import 'package:new_mails_checker/abstract_mails_checker.dart';
import 'package:new_mails_checker/account_configuration.dart';

/// Get full mail information
///
/// @author Pierre HUBERT

class FullMailGetter extends AbstractMailsChecker {
  final int uid;

  FullMailGetter({@required AccountConfiguration conf, @required this.uid})
      : assert(uid != null),
        super(conf);

  bool _requestedEmail = false;
  MailInfo _mail;
  String _beginStr = "";
  Completer<MailInfo> _completer = Completer();

  Future<MailInfo> getMailInfo() async {
    super.exec();
    return _completer.future;
  }

  @override
  void done() {
    _completer.complete(_mail);
  }

  @override
  void error(String str) {
    _completer.completeError(str);
  }

  @override
  void onNewData(String str) {
    // Get mail information
    if (_mail == null) {
      if (!_requestedEmail) {
        write("UID fetch ${uid.toString()} (UID RFC822.SIZE BODY.PEEK[])");
        _requestedEmail = true;
        return;
      } else {
        _beginStr += str;

        if (!isOkPattern(_beginStr)) {
          return;
        }

        _mail = parseMailInfo(uid, _beginStr, true);
      }
    }

    logout(str);
  }
}
