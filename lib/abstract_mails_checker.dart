import 'dart:convert';
import 'dart:io';

import 'package:meta/meta.dart';
import 'package:new_mails_checker/account_configuration.dart';

/// Here is the heart of the application where the connection to IMAP
/// servers is done
///
/// @author Pierre HUBERT

class MailInfo {
  final int uid;
  final String from;
  final String subject;
  final String body;
  final String date;

  MailInfo({
    @required this.uid,
    @required this.from,
    @required this.date,
    @required this.subject,
    @required this.body,
  })  : assert(uid != null),
        assert(date != null),
        assert(from != null),
        assert(subject != null);
}

abstract class AbstractMailsChecker {
  final AccountConfiguration conf;
  Socket _socket;
  bool _signedIn = false;
  bool _selectedInbox = false;
  bool _gotQuotaRoot = false;

  int currReq = 2;

  AbstractMailsChecker(this.conf) : assert(conf != null);

  /// Get the number of unread emails in a mailbox, given a configuration
  ///
  /// Might throw in case of failure
  void exec() async {
    /// Connect to server
    if (!conf.isSecure)
      _socket = await Socket.connect(conf.address, conf.port);
    else
      _socket = await SecureSocket.connect(conf.address, conf.port,
          onBadCertificate: (a) => conf.acceptInvalidCertificates);

    _socket.listen(_onData, onError: _onReadError);
  }

  void _onData(List<int> data) {
    final str = utf8.decode(data);
    print("Server: " + str);

    // Check for errors
    if (str.startsWith(currReq.toString() + " NO")) {
      print("Error !");
      _socket.close();
      error("Error from server : " + str);
    }

    // Check for sign in request
    if (!_signedIn) {
      // Check if login succeeded
      if (!isOkPattern(str)) {
        // Send login info
        write("login \"${conf.username}\" \"${conf.password}\"");
        return;
      } else {
        print("Signed in !");
        _signedIn = true;
        currReq++;
      }
    }

    // Select inbox
    if (!_selectedInbox) {
      if (!isOkPattern(str)) {
        write("select \"INBOX\"");
        return;
      } else {
        print("Selected inbox !");
        _selectedInbox = true;
        currReq++;
      }
    }

    // Get quota root
    if (!_gotQuotaRoot) {
      if (!isOkPattern(str)) {
        write("getquotaroot \"INBOX\"");
        return;
      } else {
        print("Got quota root !");
        _gotQuotaRoot = true;
        currReq++;
      }
    }

    // Can process what is requested to do
    onNewData(str);
  }

  void onNewData(String str);

  void logout(String str) {
    // Logout
    if (!str.startsWith("* BYE"))
      write("LOGOUT");
    else {
      _socket.close();
      done();
    }
  }

  void done();

  void error(String str);

  void write(String req) {
    req = "A"+currReq.toString() + " " + req + "\n";
    print("Client: " + req);
    _socket.write(req);
  }

  void _onReadError(e) {
    print("Read error:" + e.toString());
  }

  bool isOkPattern(String str, {String cmp = ""}) {
    return str
        .split("\n")
        .reversed
        .elementAt(1)
        .startsWith("A"+currReq.toString() + " OK " + cmp);
  }

  String _extractHeader(String str, String header) {
    if (!str.contains("\n$header: ")) return "";
    return str.split("\n$header: ").last.split("\r").first;
  }

  MailInfo parseMailInfo(int uid, String str, bool parseBody) {
    // Extract body, if required
    String body;
    if (parseBody) {
      final parts = str.split("\r\n\r\n")..removeAt(0);
      body = parts.join().split(")\r").first;
    }

    return MailInfo(
        uid: uid,
        from: _extractHeader(str, "From"),
        date: _extractHeader(str, "Date"),
        subject: _extractHeader(str, "Subject"),
        body: body);
  }
}
