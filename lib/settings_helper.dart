import 'dart:convert';

import 'package:new_mails_checker/account_configuration.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Settings helper
///
/// @author Pierre HUBERT

const _ACCOUNTS_LIST_PREFERENCE = "accounts_list";

class SettingsHelper {
  static Future<List<AccountConfiguration>> getAccounts() async {
    try {
      List<AccountConfiguration> accounts = List();
      for (Map<String, dynamic> map in jsonDecode(
          (await SharedPreferences.getInstance())
              .getString(_ACCOUNTS_LIST_PREFERENCE)))
        accounts.add(AccountConfiguration.fromJSON(map));

      return accounts;
    } catch (e) {
      e.toString();
      print(
          "Could not get the list of account, maybe application has not be configured yet...");
      return List();
    }
  }

  static Future<void> setAccounts(List<AccountConfiguration> list) async {
    await (await SharedPreferences.getInstance()).setString(
        _ACCOUNTS_LIST_PREFERENCE,
        jsonEncode(list.map((f) => f.toJson()).toList()));
  }
}
