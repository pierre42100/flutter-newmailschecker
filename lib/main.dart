import 'package:flutter/material.dart';
import 'package:new_mails_checker/abstract_mails_checker.dart';
import 'package:new_mails_checker/account_configuration.dart';
import 'package:new_mails_checker/new_mails_checker.dart';
import 'package:new_mails_checker/settings_helper.dart';
import 'package:new_mails_checker/settings_route.dart';
import 'package:new_mails_checker/single_mail_route.dart';

/// Main route of the project
///
/// @author Pierre HUBERT

main() {
  runApp(Application());
}

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "New mails checker",
      home: MainRoute(),
      debugShowCheckedModeBanner: false,
    );
  }
}

/// Main route
class MainRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mails checker"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (c) => SettingsRoute()),
                ),
          )
        ],
      ),
      body: _MainRouteBody(),
    );
  }
}

/// Main body
class _MainRouteBody extends StatefulWidget {
  @override
  __MainRouteBodyState createState() => __MainRouteBodyState();
}

class __MainRouteBodyState extends State<_MainRouteBody> {
  List<AccountConfiguration> _accountsConfiguration;

  @override
  void initState() {
    super.initState();
    _refreshAccounts();
  }

  Future<void> _refreshAccounts() async {
    final accounts = await SettingsHelper.getAccounts();
    setState(() => _accountsConfiguration = accounts);
  }

  @override
  Widget build(BuildContext context) {
    if (_accountsConfiguration == null)
      return Center(child: CircularProgressIndicator());
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: RefreshIndicator(
        onRefresh: () => _refreshAccounts(),
        child: ListView.builder(
          physics: AlwaysScrollableScrollPhysics(),
          itemCount: _accountsConfiguration.length,
          itemBuilder: (c, i) => _AccountWidget(
                conf: _accountsConfiguration[i],
              ),
        ),
      ),
    );
  }
}

class _AccountWidget extends StatefulWidget {
  final AccountConfiguration conf;

  const _AccountWidget({Key key, @required this.conf})
      : assert(conf != null),
        super(key: key);

  @override
  __AccountWidgetState createState() => __AccountWidgetState();
}

class __AccountWidgetState extends State<_AccountWidget> {
  List<MailInfo> _mails;
  bool _error = false;

  void _setError(bool b) => setState(() => _error = b);

  void _setList(List<MailInfo> list) => setState(() => _mails = list);

  @override
  void initState() {
    super.initState();
    _loadMails();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _loadMails();
  }

  @override
  void didUpdateWidget(_AccountWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    _loadMails();
  }

  Future<void> _loadMails() async {
    _setError(false);
    _setList(null);
    try {
      final list = await NewMailsChecker(widget.conf).getUnreadMailsList();
      _setList(list);
    } catch (e) {
      print(e);
      print("Could not get the list of latest mails!");
      _setError(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          widget.conf.name,
          style: TextStyle(fontSize: 20),
        ),
        _buildList()
      ],
    );
  }

  Widget _buildList() {
    if (_error)
      return Card(
        color: Colors.red,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Text(
                "Could not get the list of mails !",
                style: TextStyle(color: Colors.white),
              ),
              RaisedButton(
                onPressed: () => _loadMails(),
                child: Text("Try again".toUpperCase()),
              ),
            ],
          ),
        ),
      );

    if (_mails == null)
      return Center(
        child: CircularProgressIndicator(),
      );

    if (_mails.length == 0) return Text("There is no new mail available.");

    return Column(
      children: _mails
          .map((m) => _MailEntryWidget(
                mail: m,
                account: widget.conf,
              ))
          .toList(),
    );
  }
}

class _MailEntryWidget extends StatelessWidget {
  final MailInfo mail;
  final AccountConfiguration account;

  const _MailEntryWidget({Key key, @required this.mail, @required this.account})
      : assert(mail != null),
        assert(account != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(mail.subject),
      isThreeLine: true,
      subtitle: Text("From: " + mail.from + "\nDate:" + mail.date),
      onTap: () => Navigator.of(context).push(MaterialPageRoute(
          builder: (c) => SingleMailRoute(
                basicInfo: mail,
                account: account,
              ))),
    );
  }
}
