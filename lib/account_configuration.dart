import 'package:meta/meta.dart';

/// Account configuration information
///
/// @author Pierre HUBERT

class AccountConfiguration {
  String name;
  String address;
  int port;
  String username;
  String password;
  bool isSecure;
  bool acceptInvalidCertificates;

  AccountConfiguration(
      {@required this.name,
      @required this.address,
      @required this.port,
      @required this.username,
      @required this.password,
      @required this.isSecure,
      @required this.acceptInvalidCertificates})
      : assert(name != null),
        assert(address != null),
        assert(port != null),
        assert(username != null),
        assert(password != null),
        assert(isSecure != null),
        assert(acceptInvalidCertificates != null);

  Map<String, dynamic> toJson() => {
        "name": name,
        "address": address,
        "port": port,
        "username": username,
        "password": password,
        "isSecure": isSecure,
        "acceptInvalidCertificates": acceptInvalidCertificates
      };

  AccountConfiguration.fromJSON(Map<String, dynamic> map)
      : name = map["name"],
        address = map["address"],
        port = map["port"],
        username = map["username"],
        password = map["password"],
        isSecure = map["isSecure"],
        acceptInvalidCertificates = map["acceptInvalidCertificates"];
}
