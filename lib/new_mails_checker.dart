import 'dart:async';

import 'package:new_mails_checker/abstract_mails_checker.dart';
import 'package:new_mails_checker/account_configuration.dart';

/// New mails checker
///
/// @author Pierre HUBERT

class NewMailsChecker extends AbstractMailsChecker {
  bool _wroteFetch = false;
  bool _wroteGetMail = false;
  List<int> _mailsToGet;
  String _beginStr;

  List<MailInfo> _mailsInfo = List();

  Completer<List<MailInfo>> _completer = new Completer();

  NewMailsChecker(AccountConfiguration conf) : super(conf);

  Future<List<MailInfo>> getUnreadMailsList() async {
    super.exec();
    return _completer.future;
  }

  @override
  void onNewData(String str) {
    // Fetch emails
    if (_mailsToGet == null) {
      if (!isOkPattern(str)) {
        if (!_wroteFetch) {
          write("UID fetch 1:* (FLAGS)");
          _wroteFetch = true;
        } else {
          if (_beginStr == null)
            _beginStr = str;
          else
            _beginStr = _beginStr + str;
        }
        return;
      } else {
        String data = str;
        if (_beginStr != null) {
          data = _beginStr + str;
          _beginStr = null;
        }

        _mailsToGet = List();
        for (String line in data.split("\n")) {
          if (!line.startsWith("*")) continue;

          if (line.contains("\\Seen")) continue;

          _mailsToGet.add(int.parse(line.split("UID ").last.split(" ").first));
        }

        print("OK need to get (${_mailsToGet.length} mails): ");
        _mailsToGet.forEach((i) => print("* UID " + i.toString()));

        currReq++;
      }
    }

    // Query information about the requested mails
    while (_mailsToGet.length > 0) {
      if (!isOkPattern(str) || !_wroteGetMail) {
        if (!_wroteGetMail) {
          write("UID fetch ${_mailsToGet.last.toString()} "
              "(UID RFC822.SIZE FLAGS BODY.PEEK[HEADER.FIELDS (From To Cc Bcc Subject Date Message-ID Priority X-Priority References Newsgroups In-Reply-To Content-Type Reply-To)])");
          _wroteGetMail = true;
        } else {
          if (_beginStr == null)
            _beginStr = str;
          else
            _beginStr = _beginStr + str;
        }
        return;
      } else {
        print("Received new mail");

        String data = str;
        if (_beginStr != null) {
          data = _beginStr + str;
          _beginStr = null;
        }

        _mailsInfo.add(parseMailInfo(_mailsToGet.last, data, false));

        _wroteGetMail = false;
        _mailsToGet.removeLast();
        currReq++;
      }
    }

    logout(str);
  }

  @override
  void done() {
    _completer.complete(_mailsInfo);
  }

  @override
  void error(String str) {
    _completer.completeError(str);
  }
}
