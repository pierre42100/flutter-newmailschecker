import 'package:flutter/material.dart';

/// Utilities
///
/// @author Pierre HUBERT

/// Confirm an operation
Future<bool> showConfirmBottomSheet({
  @required BuildContext context,
  @required String message,
}) async {
  assert(context != null);
  assert(message != null);
  return await showModalBottomSheet<bool>(
    context: context,
    builder: (c) => BottomSheet(
          onClosing: () {},
          builder: (b) => Padding(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Flexible(child: Text(message)),
                    RaisedButton(
                      onPressed: () => Navigator.of(c).pop(false),
                      child: Text("No".toUpperCase()),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: RaisedButton(
                        onPressed: () => Navigator.of(c).pop(true),
                        color: Colors.red,
                        textColor: Colors.white,
                        child: Text("Yes".toUpperCase()),
                      ),
                    )
                  ],
                ),
              ),
        ),
  );
}
