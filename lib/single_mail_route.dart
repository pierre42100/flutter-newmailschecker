import 'package:flutter/material.dart';
import 'package:new_mails_checker/abstract_mails_checker.dart';
import 'package:new_mails_checker/account_configuration.dart';
import 'package:new_mails_checker/full_mail_getter.dart';

/// Single mail route
///
/// @author Pierre HUBERT

class SingleMailRoute extends StatelessWidget {
  final MailInfo basicInfo;
  final AccountConfiguration account;

  SingleMailRoute({@required this.basicInfo, @required this.account})
      : assert(basicInfo != null),
        assert(account != null),
        super();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(basicInfo.subject),
      ),
      body: _SingleMailRouteBody(
        uid: basicInfo.uid,
        account: account,
      ),
    );
  }
}

class _SingleMailRouteBody extends StatefulWidget {
  final int uid;
  final AccountConfiguration account;

  const _SingleMailRouteBody(
      {Key key, @required this.uid, @required this.account})
      : assert(uid != null),
        assert(account != null),
        super(key: key);

  @override
  _SingleMailRouteBodyState createState() => _SingleMailRouteBodyState();
}

class _SingleMailRouteBodyState extends State<_SingleMailRouteBody> {
  MailInfo _mailInfo;
  bool _error;

  void setError(bool b) => setState(() => _error = b);

  @override
  void initState() {
    super.initState();
    _getList();
  }

  Future<void> _getList() async {
    setError(false);
    try {
      final mail = await FullMailGetter(conf: widget.account, uid: widget.uid)
          .getMailInfo();
      setState(() => _mailInfo = mail);
    } catch (e) {
      print(e);
      print("Could not get full message information!");
      setError(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_error)
      return Center(
        child: Card(
          color: Colors.red,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Could not get information about this mail!",
                    style: TextStyle(color: Colors.white),
                  ),
                  RaisedButton(
                    onPressed: _getList,
                    child: Text("Try again".toUpperCase()),
                  )
                ],
              ),
            ),
          ),
        ),
      );

    if (_mailInfo == null)
      return Center(
        child: CircularProgressIndicator(),
      );

    return ListView(
      children: <Widget>[
        Text("From :" + _mailInfo.from),
        Text("Date: " + _mailInfo.date),
        Divider(),
        Text(_mailInfo.body)
      ],
    );
  }
}
