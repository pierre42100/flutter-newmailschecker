import 'package:flutter/material.dart';
import 'package:new_mails_checker/account_configuration.dart';
import 'package:new_mails_checker/settings_helper.dart';
import 'package:new_mails_checker/utils.dart';

/// Settings route
///
/// @author Pierre HUBERT

class SettingsRoute extends StatefulWidget {
  @override
  _SettingsRouteState createState() => _SettingsRouteState();
}

class _SettingsRouteState extends State<SettingsRoute> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Configuration"),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.add), onPressed: () => _addConfiguration())
        ],
      ),
      body: _SettingsRouteBody(),
    );
  }

  Future<void> _addConfiguration() async {
    await SettingsHelper.setAccounts(await SettingsHelper.getAccounts()
      ..add(AccountConfiguration(
          name: "",
          address: "",
          port: 0,
          username: "",
          password: "",
          isSecure: false,
          acceptInvalidCertificates: false)));
    setState(() {});
  }
}

class _SettingsRouteBody extends StatefulWidget {
  @override
  __SettingsRouteBodyState createState() => __SettingsRouteBodyState();
}

class __SettingsRouteBodyState extends State<_SettingsRouteBody> {
  List<AccountConfiguration> _configurations;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _loadConfiguration();
  }

  @override
  void didUpdateWidget(_SettingsRouteBody oldWidget) {
    super.didUpdateWidget(oldWidget);
    _loadConfiguration();
  }

  Future<void> _loadConfiguration() async {
    final list = await SettingsHelper.getAccounts();
    setState(() => _configurations = list);
  }

  @override
  Widget build(BuildContext context) {
    if (_configurations == null)
      return Center(
        child: CircularProgressIndicator(),
      );

    return ListView.builder(
        itemCount: _configurations.length,
        itemBuilder: (c, i) => _AccountConfigurationWidget(
              configuration: _configurations[i],
              onUpdated: _doUpdate,
              onDelete: _doDelete,
            ));
  }

  Future<void> _doUpdate() async {
    await SettingsHelper.setAccounts(_configurations);
  }

  Future<void> _doDelete(AccountConfiguration conf) async {
    setState(() {
      _configurations.remove(conf);
      _doUpdate();
    });
  }
}

class _AccountConfigurationWidget extends StatefulWidget {
  final AccountConfiguration configuration;
  final void Function() onUpdated;
  final void Function(AccountConfiguration c) onDelete;

  const _AccountConfigurationWidget(
      {Key key,
      @required this.configuration,
      @required this.onUpdated,
      @required this.onDelete})
      : assert(configuration != null),
        assert(onUpdated != null),
        assert(onDelete != null),
        super(key: key);

  @override
  __AccountConfigurationWidgetState createState() =>
      __AccountConfigurationWidgetState();
}

class __AccountConfigurationWidgetState
    extends State<_AccountConfigurationWidget> {
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _portController = TextEditingController();
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();

  @override
  void initState() {
    super.initState();
    refresh();
  }

  void refresh() {
    _nameController.text = widget.configuration.name;
    _addressController.text = widget.configuration.address;
    _portController.text = widget.configuration.port.toString();
    _userNameController.text = widget.configuration.username;
    _passwordController.text = widget.configuration.password;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          // Connection name
          _SettingsTextInput(
            label: "Connection name",
            controller: _nameController,
            onUpdate: () => _doUpdate(),
            suffixIcon:
                IconButton(icon: Icon(Icons.delete), onPressed: _deleteEntry),
          ),
          // Server name
          _SettingsTextInput(
            label: "Server name",
            controller: _addressController,
            inputType: TextInputType.url,
            onUpdate: () => _doUpdate(),
          ),

          // Server port
          _SettingsTextInput(
            label: "Server port",
            controller: _portController,
            inputType:
                TextInputType.numberWithOptions(signed: false, decimal: false),
            onUpdate: () => _doUpdate(),
          ),
          // Server user name
          _SettingsTextInput(
            label: "Username",
            controller: _userNameController,
            onUpdate: () => _doUpdate(),
          ),
          // Server password
          _SettingsTextInput(
              label: "Password",
              obscureText: true,
              controller: _passwordController,
              onUpdate: () => _doUpdate()),
          CheckboxListTile(
              value: widget.configuration.isSecure,
              title: Text("Secure connection"),
              controlAffinity: ListTileControlAffinity.leading,
              onChanged: (b) => _doUpdate(reverseSecure: true)),
          CheckboxListTile(
              value: widget.configuration.acceptInvalidCertificates,
              title: Text("Accept invalid certificates"),
              controlAffinity: ListTileControlAffinity.leading,
              onChanged: (b) => _doUpdate(reverseInvalidCertificates: true)),
          Divider(),
        ],
      ),
    );
  }

  Future<void> _doUpdate(
      {bool reverseSecure = false,
      bool reverseInvalidCertificates = false}) async {
    widget.configuration.name = _nameController.text;
    widget.configuration.address = _addressController.text;
    widget.configuration.port =
        int.parse(_portController.text.split(".")[0].split(",")[0]);
    widget.configuration.username = _userNameController.text;
    widget.configuration.password = _passwordController.text;
    if (reverseSecure)
      widget.configuration.isSecure = !widget.configuration.isSecure;
    if (reverseInvalidCertificates)
      widget.configuration.acceptInvalidCertificates =
          !widget.configuration.acceptInvalidCertificates;
    setState(() {});
    widget.onUpdated();
  }

  void _deleteEntry() async {
    if (!await showConfirmBottomSheet(
      context: context,
      message: "Do you really want to delete this configuration ?",
    )) return;
    widget.onDelete(widget.configuration);
  }
}

class _SettingsTextInput extends StatelessWidget {
  final String label;
  final TextEditingController controller;
  final TextInputType inputType;
  final void Function() onUpdate;
  final Widget suffixIcon;
  final bool obscureText;

  const _SettingsTextInput({
    Key key,
    @required this.label,
    @required this.controller,
    @required this.onUpdate,
    this.inputType,
    this.suffixIcon,
    this.obscureText = false,
  })  : assert(label != null),
        assert(controller != null),
        assert(onUpdate != null),
        assert(obscureText != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      keyboardType: inputType,
      onChanged: (s) => onUpdate(),
      obscureText: obscureText,
      decoration: InputDecoration(
          alignLabelWithHint: true, labelText: label, suffixIcon: suffixIcon),
    );
  }
}
